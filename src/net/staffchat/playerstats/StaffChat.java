package net.staffchat.playerstats;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.plugin.java.JavaPlugin;

public class StaffChat extends JavaPlugin implements Listener {
	
	//DEFAULT PERMISSION REQUIRED
	private static String permissionRequired;
	
	@Override
	public void onEnable() {
		getServer().getPluginManager().registerEvents(this, this);
		//Here, we can add something along the lines of:
		saveDefaultConfig();
		permissionRequired = getConfig().getString("permission_required") == null ? "sc.see" : getConfig().getString("permission_required");
	}
	
	//We're declaring an async chat event!
	@EventHandler
	private void chat(AsyncPlayerChatEvent e) {
		if(e.getMessage().startsWith("@")) {
			
			if(e.getPlayer().hasPermission(permissionRequired)) {
	
				//TODO: post staff chat message
			
				e.setCancelled(true);
				
				Bukkit.broadcast(ChatColor.RED + "[StaffChat]" + ChatColor.GRAY + e.getPlayer().getName() + ChatColor.RED + " > " + ChatColor.WHITE + e.getMessage().substring(1,0), permissionRequired);
			
			}
		}
	}

}
